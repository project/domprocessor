<?php

namespace Drupal\domprocessor\UrlProcessor;

use Drupal\Core\Render\HtmlResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;

interface UrlProcessorInterface {

  public function applies(HtmlResponse $response): bool;

  public function processUrl(\DomAttr $node, HtmlResponse $response): void;

  public function processRedirectResponse(RedirectResponse $response): void;

}
