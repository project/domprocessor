<?php

namespace Drupal\domprocessor\UrlProcessor;

use Drupal\domprocessor\UrlProcessor\UrlProcessorInterface;

interface ChainedUrlProcessorInterface extends UrlProcessorInterface {

  /**
   * Add processor.
   *
   * @param \Drupal\domprocessor\UrlProcessor\UrlProcessorInterface $processor
   *   The processor.
   */
  public function addProcessor(UrlProcessorInterface $processor);

}
