<?php

namespace Drupal\domprocessor\UrlProcessor;

use Drupal\Core\Render\HtmlResponse;
use Drupal\domprocessor\DomProcessor\DomProcessorInterface;

class DomProcessorForUrlProcessorManager implements DomProcessorInterface {

  private UrlProcessorInterface $urlProcessorManager;

  public function __construct(UrlProcessorInterface $urlProcessorManager) {
    $this->urlProcessorManager = $urlProcessorManager;
  }

  public function applies(HtmlResponse $response): bool {
    return $this->urlProcessorManager->applies($response);
  }

  public function processDom(\DOMDocument $dom, HtmlResponse $response): void {
    $xpath = new \DOMXPath($dom);
    // This xpath does not eat braces, so...
    $urlList = $xpath->query('//body//a/@href | //body//form/@action |//body//input/@formaction | //body//button/@formaction');

    if (!$urlList) {
      return;
    }

    foreach ($urlList as $urlNode) {
      assert($urlNode instanceof \DomAttr);
      $this->urlProcessorManager->processUrl($urlNode, $response);
    }
  }

}
