<?php

namespace Drupal\domprocessor\UrlProcessor;

use Drupal\Core\Render\HtmlResponse;
use Drupal\domprocessor\UrlProcessor\ChainedUrlProcessorInterface;
use Drupal\domprocessor\UrlProcessor\UrlProcessorInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

class UrlProcessorManager implements ChainedUrlProcessorInterface {

  /**
   * The processors.
   *
   * @var \Drupal\domprocessor\UrlProcessor\UrlProcessorInterface[]
   */
  private array $processors = [];

  /**
   * Add processor.
   *
   * @param \Drupal\domprocessor\UrlProcessor\UrlProcessorInterface $processor
   *   The processor.
   */
  public function addProcessor(UrlProcessorInterface $processor) {
    $this->processors[] = $processor;
  }

  public function applies(HtmlResponse $response): bool {
    foreach ($this->processors as $processor) {
      if ($processor->applies($response)) {
        return TRUE;
      }
    }
    return FALSE;
  }

  public function processUrl(\DomAttr $node, HtmlResponse $response): void {
    foreach ($this->processors as $processor) {
      $processor->processUrl($node, $response);
    }
  }

  public function processRedirectResponse(RedirectResponse $response): void {
    foreach ($this->processors as $processor) {
      $processor->processRedirectResponse($response);
    }
  }

}
