<?php

namespace Drupal\domprocessor\DomProcessor;

use Drupal\Core\Render\HtmlResponse;

class DomProcessorManager implements DomProcessorInterface {

  /**
   * The processors.
   *
   * @var \Drupal\domprocessor\DomProcessor\DomProcessorInterface[]
   */
  private $processors = [];

  /**
   * Add processor.
   *
   * @param \Drupal\domprocessor\DomProcessor\DomProcessorInterface $processor
   *   The processor.
   */
  public function addProcessor(DomProcessorInterface $processor) {
    $this->processors[] = $processor;
  }

  /**
   * @inheritDoc
   */
  public function applies(HtmlResponse $response): bool {
    foreach ($this->processors as $processor) {
      if ($processor->applies($response)) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * @inheritDoc
   */
  public function processDom(\DOMDocument $dom, HtmlResponse $response): void {
    foreach ($this->processors as $processor) {
      if ($processor->applies($response)) {
        $processor->processDom($dom, $response);
      }
    }
  }

}
