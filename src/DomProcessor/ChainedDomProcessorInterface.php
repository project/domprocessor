<?php

namespace Drupal\domprocessor\DomProcessor;

use Drupal\domprocessor\DomProcessor\DomProcessorInterface;

interface ChainedDomProcessorInterface extends DomProcessorInterface {

  /**
   * Add processor.
   *
   * @param \Drupal\domprocessor\DomProcessor\DomProcessorInterface $processor
   *   The processor.
   */
  public function addProcessor(DomProcessorInterface $processor);

}
