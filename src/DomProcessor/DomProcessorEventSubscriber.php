<?php

namespace Drupal\domprocessor\DomProcessor;

use Drupal\Core\Render\HtmlResponse;
use Drupal\domprocessor\UrlProcessor\UrlProcessorInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class DomProcessorEventSubscriber implements EventSubscriberInterface {

  private DomProcessorInterface $domProcessorManager;

  private UrlProcessorInterface $domUrlProcessorManager;

  public function __construct(DomProcessorInterface $domProcessorManager, UrlProcessorInterface $domUrlProcessorManager) {
    $this->domProcessorManager = $domProcessorManager;
    $this->domUrlProcessorManager = $domUrlProcessorManager;
  }

  public static function getSubscribedEvents() {
    return [
      KernelEvents::RESPONSE => [['processResponse', 0]],
    ];
  }

  /**
   * Passes HtmlResponse responses on to other functions if enabled.
   *
   * @param \Symfony\Component\HttpKernel\Event\ResponseEvent $event
   *   The event to process.
   */
  public function processResponse(ResponseEvent $event) {
    $response = $event->getResponse();
    if ($response instanceof HtmlResponse) {
      if ($this->domProcessorManager->applies($response)) {
        $html = $response->getContent();

        $dom = new \DOMDocument();
        $domOptions = LIBXML_HTML_NODEFDTD | LIBXML_NOERROR | LIBXML_NOWARNING | LIBXML_NONET | LIBXML_NOENT;
        $dom->loadHTML($html, $domOptions);
        $this->domProcessorManager->processDom($dom, $response);
        $finalHtml = $dom->saveHTML();
        $response->setContent($finalHtml);
      }
    }
    elseif ($response instanceof RedirectResponse) {
      $this->domUrlProcessorManager->processRedirectResponse($response);
    }
  }

}
