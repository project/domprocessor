<?php

namespace Drupal\domprocessor\DomProcessor;

use Drupal\Core\Render\HtmlResponse;

interface DomProcessorInterface {

  public function applies(HtmlResponse $response): bool;

  public function processDom(\DOMDocument $dom, HtmlResponse $response): void;

}
