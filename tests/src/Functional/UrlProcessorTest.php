<?php

declare(strict_types=1);

namespace Drupal\Tests\domprocessor\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;

/**
 * UrlProcessor test.
 *
 * @group domprocessor
 */
final class UrlProcessorTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'domprocessor',
    'domprocessor_test',
  ];

  /**
   * Theme to enable.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * Tests one URL has the test suffix.
   */
  public function testUrlProcessed() {
    $this->drupalGet(Url::fromRoute('user.login'));
    $this->assertSession()->linkByHrefExists('?yay=1#main-content');
  }

  /**
   * Tests form action URL has the test suffix.
   */
  public function testFormActionProcessed() {
    $this->drupalGet(Url::fromRoute('user.login'));
    $this->formActionByHrefExists('/user/login?yay=1');
  }

  protected function formActionByHrefExists($href, $index = 0, $message = '') {
    $xpath = $this->assertSession()->buildXPathQuery('//form[contains(@action, :href)]', [':href' => $href]);
    $message = ($message ? $message : strtr('Form action containing href %href found.', ['%href' => $href]));
    $links = $this->getSession()->getPage()->findAll('xpath', $xpath);
    $this->assertNotEmpty($links[$index], $message);
  }

}
