<?php

declare(strict_types=1);

namespace Drupal\Tests\domprocessor\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;

/**
 * DomProcessor test.
 *
 * @group domprocessor
 */
final class DomProcessorTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'domprocessor',
    'domprocessor_test',
  ];

  /**
   * Theme to enable.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * Tests page title has the test suffix.
   */
  public function testDomProcessed() {
    $this->drupalGet(Url::fromRoute('user.login'));
    $this->assertSession()->titleEquals('Log in | Drupal - Yay!');
  }

}
