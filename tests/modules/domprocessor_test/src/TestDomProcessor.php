<?php

declare(strict_types=1);

namespace Drupal\domprocessor_test;

use Drupal\Core\Render\HtmlResponse;
use Drupal\domprocessor\DomProcessor\DomProcessorInterface;

/**
 * Test DomProcessor appends " - Yay!" to title on all pages.
 */
final class TestDomProcessor implements DomProcessorInterface {

  public function applies(HtmlResponse $response): bool {
    return TRUE;
  }

  public function processDom(\DOMDocument $dom, HtmlResponse $response): void {
    $xPath = new \DOMXPath($dom);
    $nodeList = $xPath->query('//title');
    /** @var \DOMNode $node */
    foreach ($nodeList as $node) {
      $node->appendChild($dom->createTextNode(' - Yay!'));
    }
  }

}
