<?php

declare(strict_types=1);

namespace Drupal\domprocessor_test;

use Drupal\Core\Render\HtmlResponse;
use Drupal\domprocessor\UrlProcessor\UrlProcessorInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Test UrlProcessor adds '?yay=1' to all links on all pages.
 */
final class TestUrlProcessor implements UrlProcessorInterface {

  public function applies(HtmlResponse $response): bool {
    return TRUE;
  }

  public function processUrl(\DomAttr $node, HtmlResponse $response): void {
    $node->value = $this->addQuery($node->value, ['yay' => 1]);
  }

  public function processRedirectResponse(RedirectResponse $response): void {
    $response->setTargetUrl($this->addQuery($response->getTargetUrl(), ['yay' => 1]));
  }

  private function addQuery(string $href, array $query): string {
    $queryString = http_build_query($query);
    $querySeparator = (strpos($href, '?') !== FALSE) ? '&' : '?';
    [$hrefPath, $hrefFragmentPart] = explode('#', $href, 2) + [NULL, NULL];
    $hrefFragment = isset($hrefFragmentPart) ? "#$hrefFragmentPart" : '';
    return $hrefPath . $querySeparator . $queryString . $hrefFragment;
  }

}
